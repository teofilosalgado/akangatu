## Akangatu

Este projeto foi desenvolvido durante parte dos meus 1º e 2º anos do ensino técnico, referentes aos anos de 2015 e 2016. Foi o primeiro projeto acadêmico que fiz parte e foi uma ótima experiência do começo a fim, portanto, era mais que necessário tê-lo aqui no meu bitbucket.

A função principal desse software era substituir os antigos controladores de bancadas de ensino de eletrônica, estes eram controlados através de porta paralela e, por conseguinte, estavam obsoletos para com os computadores atuais. Deste modo, o software aqui implementado fazia a ponte entre o computador e os módulos da bancada, passando por um Arduino ou qualquer outro controlador AVR compatível.

O harware necessário é composto por Arduino ou qualquer outro microcontrolador AVR já flasheado com o sketch Firmata provido pela arduino.cc. O microcontrolador então deve ser conectado na bancada de interesse através dos pinos do mesmo.
