﻿using Firmata.NET;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows;

namespace Akangatu
{
    public class Control
    {
        public Arduino arduino = new Arduino();
        public int step { get; set; }
        public bool right, left;
        public int stepPos = 1;

        public int pwmStep;
        public DispatcherTimer pwm;
        public DispatcherTimer sm;
        public DispatcherTimer sn;
        public void bootstrap()
        {

            pwm = new DispatcherTimer(DispatcherPriority.Normal);
            sm = new DispatcherTimer(DispatcherPriority.Normal);
            sn = new DispatcherTimer(DispatcherPriority.Normal);

            pwm.Tick += new EventHandler(PWMMethod);
            pwm.IsEnabled = false;
            sm.Tick += new EventHandler(SMMethod);
            sm.IsEnabled = false;
            sn.Interval = TimeSpan.FromMilliseconds(100);
            sn.Tick += new EventHandler(SNMethod);
            sn.IsEnabled = true;
        }
        void SNMethod(object sender, EventArgs e)
        {

            int x = arduino.digitalRead(9);
            if (x == 1)
            {
                MainWindow mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                mainWin.image.Dispatcher.Invoke(new Action(() => mainWin.image.Source = new BitmapImage(new Uri(@"on.png", UriKind.Relative))));

            }
            else
            {
                MainWindow mainWin = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;
                mainWin.image.Dispatcher.Invoke(new Action(() => mainWin.image.Source = new BitmapImage(new Uri(@"off.png", UriKind.Relative))));
            }
        }
        void PWMMethod(object sender, EventArgs e)
        {
            arduino.digitalWrite(6, 1);
            Thread.Sleep(pwmStep);
            arduino.digitalWrite(6, 0);
            Thread.Sleep(pwmStep);
        }
        void SMMethod(object sender, EventArgs e)
        {
            if (right == true)
            {
                switch (stepPos)
                {
                    case 1:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(2, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(2, 0);
                        stepPos = 2;
                        break;
                    case 2:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(3, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(3, 0);
                        stepPos = 4;
                        break;
                    case 4:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(4, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(4, 0);
                        stepPos = 8;
                        break;
                    case 8:
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(5, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(5, 0);
                        stepPos = 1;
                        break;
                }
            }
            if (left == true)
            {
                switch (stepPos)
                {
                    case 1:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(2, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(2, 0);
                        stepPos = 8;
                        break;
                    case 2:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(3, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(3, 0);
                        stepPos = 1;
                        break;
                    case 4:
                        arduino.digitalWrite(5, 0);
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(4, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(4, 0);
                        stepPos = 2;
                        break;
                    case 8:
                        arduino.digitalWrite(2, 0);
                        arduino.digitalWrite(4, 0);
                        arduino.digitalWrite(3, 0);
                        arduino.digitalWrite(5, 1);
                        Thread.Sleep(10);
                        arduino.digitalWrite(5, 0);
                        stepPos = 4;
                        break;
                }
            }
        }
    }
}
