﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO.Ports;
using Firmata.NET;
using System.Windows.Forms;

namespace Akangatu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static Control control;
        public static String currentport;
        public static Boolean status = false;
        public static String[] digitalportsoutput = new String[12];
        public static TextBlock tb;

        public MainWindow()
        {
            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                comboBox.Items.Add(port);
            }

        }
        private void toggle_Checked(object sender, RoutedEventArgs e)
        {
            if (status == false)
            {
                try
                {
                    if (comboBox.Text == "")
                    {
                        toggle.IsChecked = false;
                        System.Windows.Forms.MessageBox.Show("Please select a serial port!", "No port choosen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                        control = new Control();
                        control.arduino = new Arduino(currentport, 57600);
                        for (int i = 2; i != 14; i++)
                        {
                            if (i == 9)
                            {
                                control.arduino.pinMode(9, Arduino.INPUT);
                            }
                            control.arduino.pinMode(i, Arduino.OUTPUT);
                        }
                        for (int i = 2; i != 14; i++)
                        {
                            control.arduino.digitalWrite(i, 0);
                        }
                        control.bootstrap();
                        controllergrid.IsEnabled = true;
                        comboBox.IsEnabled = false;
                        toggle.Content = "Stop!";
                        status = true;
                        currentport = comboBox.Text;
                    }
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Please select a valid serial port!", "No port choosen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    toggle.IsChecked = false;
                }
            }
        }
        private void toggle_Unchecked(object sender, RoutedEventArgs e)
        {
            control.arduino.Close();
            if (status == true)
            {
                controllergrid.IsEnabled = false;
                comboBox.IsEnabled = true;
                toggle.Content = "Start!";
                status = false;
            }
        }

        private void button5_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 1);
        }

        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var slider = sender as Slider;
            double value = slider.Value;
            control.step = ((int)value);
            if ((int)value != 0)
            {
                control.sm.Interval = TimeSpan.FromMilliseconds((int)value);
                control.sm.IsEnabled = true;
            }
            else
            {
                control.sm.IsEnabled = false;
            }
        }

        private void slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var slider = sender as Slider;
            double value = slider.Value;
            if ((int)value != 0)
            {
                control.pwm.Interval = TimeSpan.FromMilliseconds((int)value);
                control.pwm.IsEnabled = true;
                control.pwmStep = (int)value;
            }
            else
            {
                control.arduino.digitalWrite(6, 0);
                control.pwm.IsEnabled = false;
            }

        }
        private void button8_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 1);
        }

        private void button8_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 0);
        }

        private void button5_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 0);
        }

        private void button6_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(12, 1);
        }

        private void button6_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(12, 0);
        }

        private void button7_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(13, 1);
        }

        private void button7_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(13, 0);
        }

        private void button1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 1);
            control.arduino.digitalWrite(12, 1);
        }

        private void button1_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 0);
            control.arduino.digitalWrite(12, 0);
        }

        private void button2_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 1);
            control.arduino.digitalWrite(13, 1);
        }

        private void button2_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(11, 0);
            control.arduino.digitalWrite(13, 0);
        }

        private void button3_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 1);
            control.arduino.digitalWrite(12, 1);
        }

        private void button3_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 0);
            control.arduino.digitalWrite(12, 0);
        }

        private void button4_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 1);
            control.arduino.digitalWrite(13, 1);
        }

        private void button4_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(10, 0);
            control.arduino.digitalWrite(13, 0);
        }

        private void TG1_Checked(object sender, RoutedEventArgs e)
        {
            button9.IsEnabled = false;
            button10.IsEnabled = false;
            TG2.IsEnabled = false;
            control.left = true;
        }

        private void TG1_Unchecked(object sender, RoutedEventArgs e)
        {
            button9.IsEnabled = true;
            button10.IsEnabled = true;
            TG2.IsEnabled = true;
            control.left = false;

        }

        private void TG2_Checked(object sender, RoutedEventArgs e)
        {
            button9.IsEnabled = false;
            button10.IsEnabled = false;
            TG1.IsEnabled = false;
            control.right = true;
        }

        private void TG2_Unchecked(object sender, RoutedEventArgs e)
        {
            button9.IsEnabled = true;
            button10.IsEnabled = true;
            TG1.IsEnabled = true;
            control.right = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            for (int i = 2; i != 14; i++)
            {
                control.arduino.digitalWrite(i, 0);
            }
            Environment.Exit(Environment.ExitCode);
        }

        private void button9_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (control.stepPos)
            {
                case 1:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(2, 1);
                    control.stepPos = 8;
                    break;
                case 2:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(3, 1);
                    control.stepPos = 1;
                    break;
                case 4:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(4, 1);
                    control.stepPos = 2;
                    break;
                case 8:
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(5, 1);
                    control.stepPos = 4;
                    break;
            }
        }

        private void button9_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(2, 0);
            control.arduino.digitalWrite(3, 0);
            control.arduino.digitalWrite(4, 0);
            control.arduino.digitalWrite(5, 0);
        }

        private void button10_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (control.stepPos)
            {
                case 1:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(2, 1);
                    control.stepPos = 2;
                    break;
                case 2:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(3, 1);
                    control.stepPos = 4;
                    break;
                case 4:
                    control.arduino.digitalWrite(5, 0);
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(4, 1);
                    control.stepPos = 8;
                    break;
                case 8:
                    control.arduino.digitalWrite(2, 0);
                    control.arduino.digitalWrite(4, 0);
                    control.arduino.digitalWrite(3, 0);
                    control.arduino.digitalWrite(5, 1);
                    control.stepPos = 1;
                    break;
            }
        }

        private void button10_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(2, 0);
            control.arduino.digitalWrite(3, 0);
            control.arduino.digitalWrite(4, 0);
            control.arduino.digitalWrite(5, 0);
        }

        private void button12_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(7, 1);
        }

        private void button12_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            control.arduino.digitalWrite(7, 0);
        }
    }
}
